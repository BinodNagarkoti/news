import React, { Component } from "react";
// import logo from "./logo.svg";
import "./App.css";
import News from "./Component/news";
// import {
//   FormControl,
//   InputAdornment,
//   OutlinedInput,
//   InputLabel
// } from "@material-ui/core";
class App extends Component {
  state = { query: "" };
  onChange = event => {
    const { name, value } = event.target;
    this.setState({ [name]: value });
  };
  render() {
    return (
      <div className="container">
        <div className="page-title"> Todays Special News </div>
        <div className="">
          <News />
        </div>
      </div>
    );
  }
}

export default App;
