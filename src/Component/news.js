import React, { Component } from "react";
import "../App.css";
import { API_KEY, NEWS, sortBy, from } from "../assets/conf";
import {
  FormControl,
  InputAdornment,
  OutlinedInput,
  InputLabel
} from "@material-ui/core";
import img from "../assets/img/img.jpg";
// const searchKey = "";

class News extends Component {
  constructor() {
    super();
    this.state = { error: false, query: "", articles: [] };
  }
  onChange = event => {
    const { name, value } = event.target;
    this.setState({ [name]: value }, () =>
      fetch(
        `${NEWS}${this.state.query}&from=${from}sortBy=${sortBy}apiKey=${API_KEY}`
      )
        .then(response => response.json())
        .then(results => {
          if (typeof results.articles === "undefined") {
            this.setState({
              message: `No News Found With '${value}' KeyWord`,
              error: true
            });
          } else {
            const { articles } = results;
            this.setState(
              {
                articles,
                error: false
              },
              () => console.log(this.state.articles)
            );
          }

          // console.log("while fetching", results.articles);
        })
        .catch(error => console.log("error", error))
    );
  };
  componentDidMount() {
    fetch(`${NEWS}bitcoin&from=${from}sortBy=${sortBy}apiKey=${API_KEY}`)
      .then(response => response.json())
      .then(({ articles }) =>
        this.setState({ articles }, () =>
          console.log("componentdid mount:", this.state.articles)
        )
      )
      .catch(error => console.log("error", error));
  }

  render() {
    return (
      <div className="">
        <FormControl
          style={{
            color: "white",
            borderRadius: " 10px",
            border: "1px solid",
            borderColor: "white",
            background: "rgba(169, 169, 169,0.1)",
            width: "95%",
            margin: "0 2%"
          }}
          fullWidth
          className="search"
          variant="outlined"
        >
          <InputLabel
            htmlFor="outlined-adornment-Search-News"
            style={{ color: "white", backgroundColor: "black" }}
          >
            News
          </InputLabel>
          <OutlinedInput
            style={{ color: "white" }}
            id="outlined-adornment-Search-News"
            value={this.state.query}
            name="query"
            onChange={this.onChange.bind(this)}
            startAdornment={
              <InputAdornment position="start" className="inputadorements">
                search
              </InputAdornment>
            }
            labelWidth={60}
          />
        </FormControl>
        <div className="error">
          {" "}
          <h3>{this.state.error ? this.state.message : ""}</h3>
        </div>
        <div className="news">
          {this.state.articles &&
            this.state.articles.map((article, index) => (
              <main key={index}>
                <div className="title">
                  <a href={article.url}>
                    <h3>{article.title}</h3>
                  </a>
                </div>

                <div className="description">
                  <p>
                    {article.description === null
                      ? "there's no description"
                      : article.description.slice(0, 100)}
                  </p>
                </div>
                <div className="img">
                  <img src={article.urlToImage} alt="img" />
                </div>
                <div className="extra">
                  <div className="aurthor">Aurther: {article.author} </div>
                  <div className="Source"> Sources: {article.source.name}</div>
                  <div className="publishedDate">
                    Publised At {article.publishedAt}
                  </div>
                </div>

                <article>
                  <p>
                    {article.content === null
                      ? ""
                      : article.content.slice(0, 100)}
                  </p>
                </article>
              </main>
            ))}
        </div>
      </div>
    );
  }
}

export default News;
